const { session } = require("../../neodb");

module.exports = {
  create(req, res, next) {
    const friendshipProps = {
      username: req.body.username,
      friendName: req.body.friendName
    };

    if (
      friendshipProps.username === undefined ||
      friendshipProps.friendName === undefined
    ) {
      res.status(409).json({ error: "Username or friend's username missing." });
    }

    session
      .run(
        "MATCH (u:user {username: $username}), (f:user {username: $friendName}) return u",
        friendshipProps
      )
      .then(result => {
        if (!result.records[0]) {
          res.status(409).json({ error: "At least one of the usernames does not exist." });
        } else {
          session
            .run(
              "MATCH (u:user {username: $username}), (f:user {username: $friendName}) MERGE (u)-[:Friends]-(f)",
              friendshipProps
            )
            .then(() => {
              res
                .status(201)
                .json({ message: "Relationship successfully created." });
            });
        }
      })
      .catch(next);
  },

  delete(req, res, next) {
    const friendshipProps = {
      username: req.body.username,
      friendName: req.body.friendName
    };

    if (
      friendshipProps.username === undefined ||
      friendshipProps.friendName === undefined
    ) {
      res.status(409).json({ error: "Username or friend's username missing." });
    }

    session
      .run(
        "MATCH (u:user {username: $username}), (f:user {username: $friendName}) return u",
        friendshipProps
      )
      .then(result => {
        if (!result.records[0]) {
          res.status(409).json({ error: "At least one of the usernames does not exist." });
        } else {
          session
            .run(
              "MATCH (u:user {username: $username})-[r:Friends]-(:user {username: $friendName}) DELETE r",
              friendshipProps
            )
            .then(() => {
              res.status(201).json({ message: "Relationship removed." });
            })
            .catch(next);
        }
      })
      .catch(next);
  }
};
