const Thread = require("../models/thread");
const Comment = require("../models/comment");
const request = require("request-promise-native");
const assert = require("assert");
const logger = require("../config/appconfig").logger;
const { session } = require("../../neodb");

function getAsObjects(records) {
  return records.map(record => record.toObject({ forceJSNumbers: true }))
}

module.exports = {
  index(req, res, next) {
    Thread.find({}, { comments: 0, __v: 0 })
      .then(threads => res.send(threads))
      .catch(next);
  },

  create(req, res, next) {
    const threadProps = {
      username: req.body.username,
      title: req.body.title,
      content: req.body.content
    };

    try {
      assert.equal(
        typeof threadProps.username,
        "string",
        "username is not a string."
      );
      assert.equal(
        typeof threadProps.content,
        "string",
        "content is not a string."
      );
      assert.equal(
        typeof threadProps.title,
        "string",
        "title is not a string."
      );
    } catch (ex) {
      const errorObject = {
        message: "Validation fails: " + ex.toString(),
        code: 500
      };
      return next(errorObject);
    }

    const url = `${req.protocol}://${req.get("Host")}/api/users/${
      threadProps.username
      }`;

    request(url)
      .then(() => Thread.create(threadProps))
      .then(thread => res.send(thread))
      .catch(function (err) {
        const errorObject = {
          message: "User does not exist: " + err.toString(),
          code: 404
        };
        return next(errorObject);
      });
  },

  read(req, res, next) {
    const threadId = req.params.id;

    Thread.findById(threadId)
      .then(thread => res.send(thread))
      .catch(function (err) {
        const errorObject = {
          message: "Thread does not exist: " + err.toString(),
          code: 404
        };
        return next(errorObject);
      });
  },

  edit(req, res, next) {
    const threadId = req.params.id;
    const threadProps = {
      content: req.body.content
    };

    Thread.findByIdAndUpdate(threadId, threadProps, { new: true })
      .then(thread => res.send("Thread succesfully updated: " + thread))
      .catch(function (err) {
        const errorObject = {
          message: "Thread does not exist: " + err.toString(),
          code: 404
        };
        return next(errorObject);
      });
  },
  delete(req, res, next) {
    const threadId = req.params.id;

    Thread.findById(threadId)
      .then(thread => thread.remove())
      .then(() =>
        res.status(200).send("Thread succesfully removed: " + threadId)
      )
      .catch(function (err) {
        const errorObject = {
          message: "Thread does not exist: " + err.toString(),
          code: 404
        };
        return next(errorObject);
      });
  },

  getcomments(req, res, next) {
    const threadId = req.params.id;

    Thread.findById(threadId)
      .populate("comments")
      .orFail(() => res.status(404).send("Thread not found: " + threadId))
      .then(thread => res.send(thread))
      .catch(function (err) {
        const errorObject = {
          message: "User does not exist: ",
          code: 404
        };
        return next(errorObject);
      });
  },

  postcomment(req, res, next) {
    const threadId = req.params.id;
    const commentProps = {
      username: req.body.username,
      content: req.body.content,
      thread: threadId
    };

    let newCommentId;

    const url = `${req.protocol}://${req.get("Host")}/api/users/${
      commentProps.username
      }`;

    logger.info(url);

    request
      .get(url)
      .then(result => {
        if (result == "[]") {
        }
      })
      .then(() => Thread.findById(threadId).orFail(() => Error("Not found")))
      .then(() => Comment.create(commentProps))
      .then(comment => {
        newCommentId = comment._id;
      })
      .then(() =>
        Thread.findByIdAndUpdate(
          threadId,
          {
            $push: {
              comments: newCommentId
            }
          },
          { new: true }
        )
      )
      .then(() => res.redirect("/api/comments/" + newCommentId))
      .catch(next);
  },

  upvote(req, res, next) {
    const threadId = req.params.id;
    const threadPropUser = req.body.username;

    const conditions = {
      _id: threadId,
      "upvotes.username": {
        $ne: threadPropUser
      }
    };

    const update = {
      $addToSet: {
        upvotes: threadPropUser
      },
      $pull: {
        downvotes: threadPropUser
      }
    };

    const url = `${req.protocol}://${req.get(
      "Host"
    )}/api/users/${threadPropUser}`;

    request
      .get(url)
      .then(result => {
        if (result == "[]") {
          throw new Error("User does not exist.");
        }
      })
      .then(() =>
        Thread.findOneAndUpdate(conditions, update, { new: true }).orFail(() =>
          Error("Not found")
        )
      )
      .then(thread => res.send(thread))
      .catch(next);
  },

  downvote(req, res, next) {
    const threadId = req.params.id;
    const threadPropUser = req.body.username;

    const conditions = {
      _id: threadId,
      "downvotes.username": {
        $ne: threadPropUser
      }
    };

    const update = {
      $addToSet: {
        downvotes: threadPropUser
      },
      $pull: {
        upvotes: threadPropUser
      }
    };

    const url = `${req.protocol}://${req.get(
      "Host"
    )}/api/users/${threadPropUser}`;

    request
      .get(url)
      .then(result => {
        if (result == "[]") {
          throw new Error("User does not exist.");
        }
      })
      .then(() =>
        Thread.findOneAndUpdate(conditions, update, { new: true }).orFail(() =>
          Error("Not found")
        )
      )
      .then(thread => res.send(thread))
      .catch(next);
  },

  // This request gets the threads by friend amount

  findByFriendAmount(req, res, next) {

    const params = {
      username: req.body.username,
      friendAmount: req.body.amount
    }

    if(params.username === undefined || params.friendAmount === undefined) {
      res.status(409).json({ "error": "Please enter a username and friend amount." });
  }

  else {
  

    session.run('MATCH (p:user{username: \"'  +  params.username + '\"})<-[:Friends*0..' + params.friendAmount + ']->(yourfriends) RETURN DISTINCT yourfriends.username as username')
      .then((result => {
        if(!result.records[0]) {
          res.status(401).json({ "error": "Username does not exist." })
        }

        else {

        var promises = []

        const objects = getAsObjects(result.records);

        for (let index = 0; index < objects.length; index++) {
          var x = Thread.findOne(objects[index])
          promises.push(x)
        }

        Promise.all(promises).then(function(results) {
          if(!results[0]){
            res.status(404).json({ "error": "This user doesn't have any threads."})
          }
          res.send(results)
        }).catch(function(err) {
          res.send(err)
        })
      }
    }))
  }
  }
};
