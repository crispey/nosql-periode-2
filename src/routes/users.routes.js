const express = require("express");
const router = express.Router();
const UserController = require("../controllers/user.controller");

router.get("/", UserController.index);
router.post("/", UserController.create);
router.put("/:username", UserController.edit);
router.get("/:username", UserController.read);
router.delete("/:username", UserController.delete);

module.exports = router;
