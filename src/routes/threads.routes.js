const express = require("express");
const router = express.Router();
const ThreadController = require("../controllers/thread.controller");

router.get("/", ThreadController.index);
router.post("/", ThreadController.create);

router.get("/friends", ThreadController.findByFriendAmount);

router.get("/:id", ThreadController.read);
router.put("/:id", ThreadController.edit);
router.delete("/:id", ThreadController.delete);

router.get("/:id/comments", ThreadController.getcomments);
router.post("/:id/comments", ThreadController.postcomment);

router.post('/:id/upvotes', ThreadController.upvote)
router.post('/:id/downvotes', ThreadController.downvote)

module.exports = router;
