const express = require("express");
const router = express.Router();
const FriendshipController = require("../controllers/friendship.controller");

router.post("/", FriendshipController.create);
router.delete("/", FriendshipController.delete);

module.exports = router;
