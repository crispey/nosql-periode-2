const express = require("express");
const router = express.Router();
const commentController = require("../controllers/comment.controller");

router.get("/:id", commentController.read);
router.put("/:id", commentController.edit);
router.delete("/:id", commentController.delete);

router.get("/:id/comments", commentController.replies);
router.post("/:id/comments", commentController.reply);

router.post("/:id/upvotes", commentController.upvote);
router.post("/:id/downvotes", commentController.downvote);

module.exports = router;
