const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
const expect = chai.expect();
const app = require("../app");

let threadId;
let commentId;

const username = "Test123";
const password = "1234";
const nonExistingUsername = "Comment Test User X";

const title = "Comment Test Title";
const content = "Lorem ipsum.";
const updatedContentTemplate = "Edit: Test.";
const updatedContent = content + " " + updatedContentTemplate;
const commentContent = "Very nice!";
const updatedCommentContentTemplate = "Edit: Test.";
const updatedCommentContent =
  commentContent + " " + updatedCommentContentTemplate;

  chai.use(chaiHttp)
  describe('Comment API interface', () => {
      before(function (done) {
          this.timeout(10000);
  
          chai.request(app)
              .post('/api/users')
              .send({
                  username: username,
                  password: password
              })
              .end(function (err, res) {
                  chai.request(app)
                      .get('/api/users/' + username)
                      .end(function (err, res) {
                          res.should.have.status(200);
                          res.body.should.have.property('username');
                          res.text.should.contain(username)
  
                          chai.request(app)
                              .post('/api/threads')
                              .send({
                                  username: username,
                                  title: title,
                                  content: content
                              })
                              .end(function (err, res) {
                                  res.should.have.status(200);
                                  res.body.should.have.property('_id');
                                  threadId = res.body._id;
  
                                  chai.request(app)
                                      .post('/api/threads/' + threadId + '/comments')
                                      .send({
                                          username: username,
                                          content: commentContent,
                                      })
                                      .end(function (err, res) {
                                          res.should.have.status(200)
                                          res.body.should.be.a('object')
                                          res.text.should.contain(commentContent)
                                          commentId = res.body._id;
  
                                          done()
                                      })
                              })
                      })
              })
      })
      after(function (done) {
          this.timeout(10000);
  
          chai.request(app)
              .delete('/api/threads/' + threadId)
              .end(function (err, res) {
                  res.should.have.status(200);
  
                  chai.request(app)
                      .delete('/api/users/' + username)
                      .send({
                          password: password
                      })
                      .end(function (err, res) {
                          res.should.have.status(204);
                          done();
                      })
              })
      })
  
      it('should get a specific comment', function (done) {
          chai.request(app)
              .get('/api/comments/' + commentId)
              .end(function (err, res) {
                  res.should.have.status(200);
                  res.body.should.be.a('object')
                  res.text.should.contain(username)
                  res.text.should.contain(commentContent)
                  done()
              })
      })
      it('should update a comment', function (done) {
          chai.request(app)
              .put('/api/comments/' + commentId)
              .send({
                  content: updatedCommentContent
              })
              .end(function (err, res) {
                  res.should.have.status(200)
                  res.body.should.be.a('object')
                  res.text.should.contain(updatedCommentContentTemplate)
                  done()
              })
      })
})
  