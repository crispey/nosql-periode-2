const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const expect = chai.expect();
const app = require('../app');

let threadId;
let threadId1;

const username = "Thread Test User";
const password = "9216517133";
const nonExistingUsername = "Thread Test User X";

const friendUsername = "Friend User"
const friendAmount = "1"


const title = "Thread Test Title";
const updatedTitle = "Thread Test Title 2";
const content1 = "Hello test"
const content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu rutrum risus.";
const updatedContentTemplate = "Edit: Test.";
const falseContent = "Something else";
const updatedContent = content + " " + updatedContentTemplate;
const commentContent = "Very nice!";

chai.use(chaiHttp)
describe('Thread API interface', () => {
    before(function (done) {
        this.timeout(10000);
        chai.request(app)
            .post('/api/users')
            .send({
                username: username,
                password: password
            })
            .end(function (err, res) {
                chai.request(app)
                    .get('/api/users/' + username)
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.should.have.property('username');
                        res.text.should.contain(username);

                        chai.request(app)
                            .post('/api/users')
                            .send({
                                username: friendUsername,
                                password: password
                            })
                            .end(function (err, res) {
                                chai.request(app)
                                    .get('/api/users/' + friendUsername)
                                    .end(function (err, res) {
                                        res.should.have.status(200);
                                        res.body.should.have.property('username');
                                        res.text.should.contain(password)

                                        chai.request(app)
                                            .post('/api/friendships')
                                            .send({
                                                username: username,
                                                friendName: friendUsername
                                            })
                                            .end((err, res) => {
                                                res.should.have.status(201);
                                                done();
                                            });
                                    })
                            })
                    })
            })
    })

    after(function (done) {
        chai.request(app)
            .delete('/api/friendships')
            .send({
                username: username,
                friendName: friendUsername
            })
            .end((err, res) => {
                res.should.have.status(201);
                chai.request(app)
                    .delete('/api/users/' + username)
                    .send({
                        password: password
                    })
                    .end(function (err, res) {
                        res.should.have.status(204);

                        chai.request(app)
                            .delete('/api/users/' + friendUsername)
                            .send({
                                password: password
                            })
                            .end(function (err, res) {
                                res.should.have.status(204);
                                done();
                            })
                    })
            })
    })

    it('should get all threads', function (done) {
        chai.request(app)
            .get('/api/threads')
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('array');
                done()
            })
    })
    it('should create a thread', function (done) {
        chai.request(app)
            .post('/api/threads')
            .send({
                username: username,
                title: title,
                content: content
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.have.property('_id');
                threadId = res.body._id;
                done()
            })
    })
    it('Integration - created thread1', function (done) {
        chai.request(app)
            .post('/api/threads')
            .send({
                username: username,
                title: title,
                content: content1
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.have.property('_id');
                threadId1 = res.body._id;
                done()
            })
    })
    it('Integration - Thread1 should be available', function (done) {
        chai.request(app)
            .get('/api/threads/' + threadId1)
            .end(function (err, res) {
                res.should.have.status(200)
                res.text.should.contain(content1)
                done()
            })
    })

    it('should get all threads, including created one', function (done) {
        chai.request(app)
            .get('/api/threads')
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('array')
                res.text.should.contain(title)
                res.text.should.contain(content)

                // Checking for proper content in first element of thread list
                res.body.should.have.any.keys(0, '_id');
                res.body.should.have.any.keys(0, 'username');
                res.body.should.have.any.keys(0, 'title');
                res.body.should.have.any.keys(0, 'content');
                res.body.should.have.any.keys(0, 'upvotesCount');
                res.body.should.have.any.keys(0, 'downvotesCount');
                res.body.should.have.any.keys(0, 'comments');

                done()
            })
    })
    it('Integration - should update and test get', function (done) {
        chai.request(app)
            .put('/api/threads/' + threadId)
            .send({
                content: updatedContent
            })
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.text.should.contain(updatedContentTemplate)
            })

        chai.request(app)
            .get('/api/threads/' + threadId)
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.text.should.contain(updatedContentTemplate)
                done()
            })
    })
    it('Integration - update and get are not the same. ', function (done) {
        chai.request(app)
            .put('/api/threads/' + threadId)
            .send({
                content: updatedContent
            })
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.text.should.contain(updatedContentTemplate)
            })
        chai.request(app)
            .get('/api/threads/' + threadId)
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.text.should.not.contain(falseContent)
                done()
            })
    })
    it('should update a thread', function (done) {
        chai.request(app)
            .put('/api/threads/' + threadId)
            .send({
                content: updatedContent
            })
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.text.should.contain(updatedContentTemplate)
                done()
            })
    })
    it('should ignore an update to a thread\'s title', function (done) {
        chai.request(app)
            .put('/api/threads/' + threadId)
            .send({
                title: updatedTitle
            })
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.text.should.not.contain(updatedTitle)
                res.text.should.contain(title)
                done()
            })
    })
    it('should add an upvote on a thread', function (done) {
        chai.request(app)
            .post('/api/threads/' + threadId + '/upvotes')
            .send({
                username: username,
            })
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                chai.expect(res.body.upvotesCount).to.equal(1)
                chai.expect(res.body.downvotesCount).to.equal(0)
                done()
            })
    })
    it('should ignore a duplicate upvote on a thread', function (done) {
        chai.request(app)
            .post('/api/threads/' + threadId + '/upvotes')
            .send({
                username: username,
            })
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                chai.expect(res.body.upvotesCount).to.equal(1)
                chai.expect(res.body.downvotesCount).to.equal(0)
                done()
            })
    })
    it('should change an upvote into a downvote on a thread', function (done) {
        chai.request(app)
            .post('/api/threads/' + threadId + '/downvotes')
            .send({
                username: username,
            })
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                chai.expect(res.body.upvotesCount).to.equal(0)
                chai.expect(res.body.downvotesCount).to.equal(1)
                done()
            })
    })
    it('should ignore a duplicate downvote on a thread', function (done) {
        chai.request(app)
            .post('/api/threads/' + threadId + '/downvotes')
            .send({
                username: username,
            })
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                chai.expect(res.body.upvotesCount).to.equal(0)
                chai.expect(res.body.downvotesCount).to.equal(1)
                done()
            })
    })
    it('should add a reply on a thread', function (done) {
        chai.request(app)
            .post('/api/threads/' + threadId + '/comments')
            .send({
                username: username,
                content: commentContent,
            })
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.text.should.contain(commentContent)
                done()
            })
    })
    it('should get replies on a thread, including created one', function (done) {
        chai.request(app)
            .get('/api/threads/' + threadId + '/comments')
            .end(function (err, res) {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.text.should.contain(commentContent)
                done()
            })
    })
    it('should delete a thread', function (done) {
        chai.request(app)
            .delete('/api/threads/' + threadId)
            .end(function (err, res) {
                res.should.have.status(200);
                done();
            })
    })

    it('Integration - should delete a thread1 ', function (done) {
        chai.request(app)
            .delete('/api/threads/' + threadId1)
            .end(function (err, res) {
                res.should.have.status(200);
                done();
            })
    })

    it('Integration - deleted thread1 should be gone ', function (done) {
        chai.request(app)
            .get('/api/threads/' + threadId1)
            .end(function (err, res) {
                res.text.should.not.contain(content1)
                done();
            })
    })

    it('Should not get threads of user that does not exist', function (done) {
        chai.request(app)
            .get('/api/threads/friends')
            .send({
                username: nonExistingUsername,
                amount: friendAmount
            })
            .end(function (err, res) {
                res.should.have.status(401)
                done()
            })
    })

    it('Should not get threads of user(s) that don\'t have any threads', function (done) {
        chai.request(app)
            .get('/api/threads/friends')
            .send({
                username: friendUsername,
                amount: friendAmount
            })
            .end(function (err, res) {
                res.should.have.status(404)
                done()
            })
    })
})