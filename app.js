const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const threadRoutes = require('./src/routes/threads.routes')
const userRoutes = require('./src/routes/users.routes')
const commentRoutes = require('./src/routes/comments.routes')
const friendshipRoutes = require('./src/routes/friendships.routes')
const logger = require('./src/config/appconfig').logger
require('dotenv').config();

const app = express()
const port = process.env.PORT || 3000

mongoose.Promise = global.Promise

var mongoDB = 'mongodb+srv://user:user@cluster0-furay.azure.mongodb.net/test?retryWrites=true&w=majority';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json())

// Generic endpoint handler - voor alle routes
app.all('*', (req, res, next) => {

// logger.info('Generieke afhandeling aangeroepen!')
  const { method, url } = req
  logger.info(`${method} ${url}`)
  next()
})

// Hier installeren we de routes
app.use('/api/threads', threadRoutes)
app.use('/api/users', userRoutes)
app.use('/api/comments', commentRoutes)
app.use('/api/friendships', friendshipRoutes)

// Handle endpoint not found.
app.all('*', (req, res, next) => {
  const { method, url } = req
  const errorMessage = `${method} ${url} does not exist.`
  logger.warn(errorMessage)
  const errorObject = {
    message: errorMessage,
    code: 404,
    date: new Date()
  }
  next(errorObject)
})

// Error handler
app.use((error, req, res, next) => {
  logger.error('Error handler: ', error.message.toString())
  res.status(error.code).json(error)
})

app.listen(port, () => logger.info(`App listening on port: ${port}!`))

module.exports = app
